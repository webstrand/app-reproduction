import React from "react";

type TestComponentProps = {
    name?: string;
    address: string;
} & {
    id?: string;
    name: string;
    phone: number;
};

export class TestComponent extends React.Component<TestComponentProps> {
    static defaultProps = {
        name: 'John'
    };

    render(): React.ReactNode {
        return <div id={this.props.id}>{this.props.phone}</div>;
    }
}
