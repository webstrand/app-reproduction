import React from "react";
declare type TestComponentProps = {
    name?: string;
    address: string;
} & {
    id?: string;
    name: string;
    phone: number;
};
export declare class TestComponent extends React.Component<TestComponentProps> {
    static defaultProps: {
        name: string;
    };
    render(): React.ReactNode;
}
export {};
